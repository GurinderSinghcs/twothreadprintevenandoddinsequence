import javax.swing.plaf.nimbus.NimbusLookAndFeel;

public class PrintNumbers1ToN {
	public static void main(String[] args) {
		Object mutex = new Object();
		NumberThread even = new NumberThread(true, mutex);
		even.start();
		NumberThread odd = new NumberThread(false, mutex);
		odd.start();
	}
}
