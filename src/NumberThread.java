
public class NumberThread extends Thread{
	Object mutex = new Object();
	private boolean even;
	public NumberThread(boolean even, Object mutex) {
		this.even = even; 
		this.mutex = mutex;
	}
	public void run(){  
		if(even) {
			for(int i=2;i<=100;i = i+2) {
				synchronized (mutex) {
					try {
						mutex.wait();
					} catch (InterruptedException e) {
						
					}
				}
				
				synchronized (mutex) {
					System.out.println(i);
					mutex.notifyAll();
				}
				if(i==100) {
					break;
				}
				
			}
			
		}else {
			for(int i=1;i<100;i = i+2) {
				
				synchronized (mutex) {
					System.out.println(i);
					mutex.notifyAll();
				}
				if(i==99) {
					break;
				}
				synchronized (mutex) {
					try {
						mutex.wait();
					} catch (InterruptedException e) {
						
					}
				}
			}
		}
	}
}
